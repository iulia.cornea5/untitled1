package org.example;

import java.time.LocalDate;

public class Person {
    // id <- acesta este un comentariu
    // member variable, variabila de clasa
    // int is a primitive type.
    // other primitive types are
    // long, float, double, char, bool
    int id;
    // name
    String name;
    // birthdate
    LocalDate birthdate;
}
